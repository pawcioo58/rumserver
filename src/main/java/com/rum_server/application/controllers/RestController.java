package com.rum_server.application.controllers;

import com.rum_server.application.helpers.ResourceMap;
import com.rum_server.application.helpers.RestResponse;
import com.rum_server.application.helpers.Session;
import com.rum_server.application.models.*;
import com.rum_server.application.repositories.GroupResourceRepository;
import com.rum_server.application.repositories.UserToResourceRepository;
import com.rum_server.application.services.ResourceService;
import com.rum_server.application.services.UserService;
import com.rum_server.application.services.UserToResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.soap.SOAPBinding;
import java.util.*;

@org.springframework.web.bind.annotation.RestController   // This means that this class is a Controller
public class RestController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private UserToResourceService userToResourceService;

    @Autowired
    private GroupResourceRepository groupResourceService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    Map<Session, User> sessionToUser;


    public RestController() {
        sessionToUser = new HashMap<>();
    }

    @RequestMapping(value = {"/rest/users"}, method = RequestMethod.GET)
    public @ResponseBody
    List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = {"/rest/login"}, method = RequestMethod.GET)
    public @ResponseBody
    RestResponse getNewSession(@RequestParam(value = "login") String login,
                               @RequestParam(value = "passwd") String passwd) {
        User user = userService.findUserBySignum(login);

        if (user == null)
            return new RestResponse("User not exist.");
        boolean isCorrectPassword = bCryptPasswordEncoder.matches(passwd, user.getPassword());
        if (isCorrectPassword) {
            if (sessionToUser.values().contains(user)) {
                for (Session currentSession : sessionToUser.keySet()) {
                    if (sessionToUser.get(currentSession).equals(user))
                        return currentSession;
                }
            }
            Session newSession = new Session();
            sessionToUser.put(newSession, user);

            return newSession;
        }
        return new RestResponse("Password is incorrect");

    }


    @RequestMapping(value = {"/rest/resources"}, method = RequestMethod.GET)
    public @ResponseBody
    RestResponse getListOfResourcesSession(@RequestParam(value = "sessionID") long sessionID) {

        if (!sessionToUser.containsKey(Session.createFor(sessionID)))
            return new RestResponse("Operation not permited.");

        User currentUser = sessionToUser.get(Session.createFor(sessionID));
        User user = userService.findUserBySignum(currentUser.getSignum());
        Set<UserGroup> groupsForCurrentUser = user.getGroups();

        Map<ResourceGroup, List<ResourceForJson>> resultMap = new HashMap<>();

        for (UserGroup currentUGroup : groupsForCurrentUser) {
            int currentGroupResourceId = currentUGroup.getrGroupId();
            ResourceGroup resourceGroupForUserGroup = groupResourceService.findById(currentGroupResourceId);

            List<Resource> resourcesForUser = resourceService.findResourcesByrGroupId(currentGroupResourceId);


            List<ResourceForJson> resourceForJson = getResourcesForJson(resourcesForUser);
            resultMap.put(resourceGroupForUserGroup, resourceForJson);
        }

        return new ResourceMap(resultMap);

    }

    private List<ResourceForJson> getResourcesForJson(List<Resource> resourcesForUser) {
        List<ResourceForJson> result = new ArrayList<>();
        for (Resource current : resourcesForUser){
            ResourceForJson temp = new ResourceForJson(current);
            User user= getUserForResource(current);
            String userName = "";
            if(user!=null)
                userName=user.getSignum();
            temp.setUser(userName);
            result.add(temp);
        }
        return result;
    }

    private User getUserForResource(Resource resource) {
        List<UserToResource> list = userToResourceService.getAll();
        for(UserToResource currentItem : list){
            if(currentItem.getResource_id()==resource.getId()){
                int userId = currentItem.getUser_id();
                return userService.findUserById(userId);
            }
        }
        return null;
    }


    @RequestMapping(value = {"/rest/bookResource"}, method = RequestMethod.GET)
    public @ResponseBody
    RestResponse bookResource(@RequestParam(value = "sessionID") long sessionID, @RequestParam(value = "resource_id") int resource_id) {
        if (!sessionToUser.containsKey(Session.createFor(sessionID)))
            return new RestResponse("Operation not permitted.");

        Resource requestedResource = resourceService.findResourceById(resource_id);
        if(requestedResource==null){
            return new RestResponse("Resource with this ID not exist in database.");
        }

        User currentUser = sessionToUser.get(Session.createFor(sessionID));
        User user = userService.findUserBySignum(currentUser.getSignum());
        Set<UserGroup> groupsForCurrentUser = user.getGroups();

        List<Resource> resources = new ArrayList<>();

        for (UserGroup currentUGroup : groupsForCurrentUser) {
            int currentGroupResourceId = currentUGroup.getrGroupId();
            List<Resource> resourcesForUser = resourceService.findResourcesByrGroupId(currentGroupResourceId);
            resources.addAll(resourcesForUser);
        }

        if(!resources.contains(requestedResource)){
            return new RestResponse("Operation not permitted.");
        }

        if(requestedResource.getrState()==0)
            return new RestResponse("Error. Resource is already booked.");

        requestedResource.setrState(0);
        resourceService.book(requestedResource);
        userToResourceService.book(user,requestedResource);
        return new RestResponse("OK");
    }


    @RequestMapping(value = {"/rest/releaseResource"}, method = RequestMethod.GET)
    public @ResponseBody
    RestResponse releaseResource(@RequestParam(value = "sessionID") long sessionID, @RequestParam(value = "resource_id") int resource_id) {
        if (!sessionToUser.containsKey(Session.createFor(sessionID)))
            return new RestResponse("Operation not permited.");

        Resource requestedResource = resourceService.findResourceById(resource_id);
        if(requestedResource==null){
            return new RestResponse("Resource with this ID not exist in database.");
        }

        User currentUser = sessionToUser.get(Session.createFor(sessionID));
        User user = userService.findUserBySignum(currentUser.getSignum());
        Set<UserGroup> groupsForCurrentUser = user.getGroups();

        List<Resource> resources = new ArrayList<>();

        for (UserGroup currentUGroup : groupsForCurrentUser) {
            int currentGroupResourceId = currentUGroup.getrGroupId();
            List<Resource> resourcesForUser = resourceService.findResourcesByrGroupId(currentGroupResourceId);
            resources.addAll(resourcesForUser);
        }

        if(!resources.contains(requestedResource)){
            return new RestResponse("Operation not permitted.");
        }

        if(requestedResource.getrState()==1)
            return new RestResponse("Error. Resource is already released.");

        requestedResource.setrState(1);
        resourceService.release(requestedResource);
        userToResourceService.release(user,requestedResource);
        return new RestResponse("OK");
    }



}
