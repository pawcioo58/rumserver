package com.rum_server.application.models;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "resource")
public class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "rName")
    private String rName;

    @Column(name = "rState")
    private int rState;

    @Column(name = "rDescription")
    private String rDescription;

    @Column(name = "rGroupId")
    private int rGroupId;

    public Resource(){

    }

    public Resource(int id, String rName, int rState, String rDescription, int rGroupId) {
        this.id = id;

        this.rName = rName;

        this.rState = rState;

        this.rDescription = rDescription;

        this.rGroupId = rGroupId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getrName() {
        return rName;
    }

    public void setrName(String rName) {
        this.rName = rName;
    }

    public int getrState() {
        return rState;
    }

    public void setrState(int rState) {
        this.rState = rState;
    }

    public String getrDescription() {
        return rDescription;
    }

    public void setrDescription(String rDescription) {
        this.rDescription = rDescription;
    }

    public int getrGroupId() {
        return rGroupId;
    }

    public void setrGroupId(int rGroupId) {
        this.rGroupId = rGroupId;
    }
}
