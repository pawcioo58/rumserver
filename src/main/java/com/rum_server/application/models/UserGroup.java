package com.rum_server.application.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "userGroup")
public class UserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "group_id")
    private int id;

    @Column(name = "ugName")
    private String ugName;

    @Column(name = "rGroupId")
    private int rGroupId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUgName() {
        return ugName;
    }

    public void setUgName(String ugName) {
        this.ugName = ugName;
    }

    public int getrGroupId() {
        return rGroupId;
    }

    public void setrGroupId(int rGroupId) {
        this.rGroupId = rGroupId;
    }
}
