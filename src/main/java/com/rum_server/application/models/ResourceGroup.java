package com.rum_server.application.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "resourceGroup")
public class ResourceGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "gName")
    private String gName;

    @Column(name = "gDescription")
    private String gDescription;

    public ResourceGroup(){}

    public ResourceGroup(int id, String gName, String gDescription) {
        this.id = id;
        this.gName = gName;
        this.gDescription = gDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getgDescription() {
        return gDescription;
    }

    public void setgDescription(String gDescription) {
        this.gDescription = gDescription;
    }

    @Override
    public String toString() {
        return gName;
    }
}
