package com.rum_server.application.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

public class ResourceForJson {


    private int id;

    private String rName;

    private int rState;

    private String rDescription;

    private int rGroupId;

    private String user;


    public ResourceForJson(Resource resource) {
        id = resource.getId();
        rName = resource.getrName();
        rState = resource.getrState();
        rDescription = resource.getrDescription();
        rGroupId = resource.getrGroupId();
    }

    public void setUser(String name) {
        this.user = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getrName() {
        return rName;
    }

    public void setrName(String rName) {
        this.rName = rName;
    }

    public int getrState() {
        return rState;
    }

    public void setrState(int rState) {
        this.rState = rState;
    }

    public String getrDescription() {
        return rDescription;
    }

    public void setrDescription(String rDescription) {
        this.rDescription = rDescription;
    }

    public int getrGroupId() {
        return rGroupId;
    }

    public void setrGroupId(int rGroupId) {
        this.rGroupId = rGroupId;
    }

    public String getUser() {
        return user;
    }

}
