package com.rum_server.application.helpers;

import java.util.Objects;

public class Session extends RestResponse{
    long sessionID;

    public Session(long sessionId) {
        super("OK");
        sessionID = sessionId;
    }

    public Session() {
        super("OK");
        sessionID =  (System.currentTimeMillis()*100L) + ((long)Math.random() * 100L);
    }

    public static Session createFor(long sessionID) {
        return new Session(sessionID);
    }

    public long getSessionID() {
        return sessionID;
    }

    public void setSessionID(long sessionID) {
        this.sessionID = sessionID;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return sessionID == session.sessionID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionID);
    }
}
