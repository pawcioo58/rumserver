package com.rum_server.application.helpers;

import com.rum_server.application.models.Resource;
import com.rum_server.application.models.ResourceForJson;
import com.rum_server.application.models.ResourceGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceMap extends RestResponse {
    Map <ResourceGroup, List<ResourceForJson>> data;
    public ResourceMap(Map<ResourceGroup, List<ResourceForJson>> map) {
        super("OK");
        data = map;
    }

    public Map<ResourceGroup, List<ResourceForJson>> getData() {
        return data;
    }

    public void setData(Map<ResourceGroup, List<ResourceForJson>> data) {
        this.data = data;
    }
}
