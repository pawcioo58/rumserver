package com.rum_server.application.helpers;

public class RestResponse {
    String responseMessage;
    public RestResponse(String message){
        this.responseMessage = message;
    }

    public String getMessage() {
        return responseMessage;
    }

    public void setMessage(String message) {
        this.responseMessage = message;
    }
}