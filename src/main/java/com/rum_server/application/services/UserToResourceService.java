package com.rum_server.application.services;


import com.rum_server.application.models.Resource;
import com.rum_server.application.models.Role;
import com.rum_server.application.models.User;
import com.rum_server.application.models.UserToResource;
import com.rum_server.application.repositories.ResourceRepository;
import com.rum_server.application.repositories.UserToResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service("resourceToRepositoryService")
public class UserToResourceService {


    private UserToResourceRepository userToResourceRepository;

    @Autowired
    public UserToResourceService(
            UserToResourceRepository userToResourceRepository) {
        this.userToResourceRepository = userToResourceRepository;
    }

    public void book(User user, Resource resource) {
        UserToResource current = new UserToResource();
        current.setResource_id(resource.getId());
        current.setUser_id(user.getId());
        userToResourceRepository.save(current);
    }

    public void release(User user, Resource resource) {

        List<UserToResource> allResources = userToResourceRepository.findAll();
        UserToResource temp = null;
        for(UserToResource currentResource : allResources){
            if(currentResource.getResource_id() == resource.getId() && currentResource.getUser_id() == user.getId())
                temp = currentResource;
        }
        if(temp!=null)
            userToResourceRepository.delete(temp);
    }

    public List<UserToResource> getAll() {
        return userToResourceRepository.findAll();
    }
}