package com.rum_server.application.services;


import com.rum_server.application.models.Resource;
import com.rum_server.application.models.Role;
import com.rum_server.application.models.User;
import com.rum_server.application.repositories.ResourceRepository;
import com.rum_server.application.repositories.RoleRepository;
import com.rum_server.application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service("resourceService")
public class ResourceService {


    private ResourceRepository resourceRepository;

    @Autowired
    public ResourceService(
            ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    public List<Resource> findResourcesByrGroupId(int rGroupId) {
        return resourceRepository.findByrGroupId(rGroupId);
    }

    public Resource findResourceById(int resource_id) {
        return resourceRepository.findById(resource_id);
    }

    public void book(Resource requestedResource) {
        Resource resource = resourceRepository.findById(requestedResource.getId());
        resource.setrState(0);
    }

    public void release(Resource requestedResource) {
        Resource resource = resourceRepository.findById(requestedResource.getId());
        resource.setrState(1);
    }
}