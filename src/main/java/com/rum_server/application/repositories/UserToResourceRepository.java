package com.rum_server.application.repositories;

import com.rum_server.application.models.Resource;
import com.rum_server.application.models.ResourceGroup;
import com.rum_server.application.models.User;
import com.rum_server.application.models.UserToResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface UserToResourceRepository extends JpaRepository<UserToResource, Integer> {
    UserToResource findById(int id);


   // UserToResource findByResource_id(int resource_id);
}