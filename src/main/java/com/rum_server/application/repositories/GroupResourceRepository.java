package com.rum_server.application.repositories;

import com.rum_server.application.models.Resource;
import com.rum_server.application.models.ResourceGroup;
import com.rum_server.application.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("groupResourceRepository")
public interface GroupResourceRepository extends JpaRepository<ResourceGroup, Integer> {
    ResourceGroup findById(int id);

}