package com.rum_server.application.repositories;

import com.rum_server.application.models.Resource;
import com.rum_server.application.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("resourceRepository")
public interface ResourceRepository extends JpaRepository<Resource, Integer> {

    List<Resource> findByrGroupId(int id);
    Resource findById(int id);

}