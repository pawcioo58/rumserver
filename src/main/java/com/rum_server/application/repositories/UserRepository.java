package com.rum_server.application.repositories;

import com.rum_server.application.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
    User findById(int id);
    User findBySignum(String signum);
}